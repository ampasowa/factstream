#!/usr/bin/env python
# Parses json formatted play-by-play Game data from SportRadar
# and writes output to cassandra database
#
# Usage: ./data_parser <play-by-play_json_file>
#
# Table Schema
#
# CREATE TABLE game_data.events(
#     period text,
#     clock int,
#     team_id text,
#     event_type text,
#     game_id text,
#     event_id text,
#     ast int,
#     tov int,
#     stl int,
#     trb int,
#     three int,
#     ft int,
#     pt int,
#     two int,
#     PRIMARY KEY (period, clock, team_id, event_type, game_id, event_id)
# );


import sys
import json
import six
from cassandra.cluster import Cluster

cluster = Cluster(['127.0.0.1'])
session = cluster.connect('game_data')

interesting_events = ['freethrowmade', 'twopointmade', 'threepointmade',
                        'rebound', 'turnover']
class TeamStats(object):
    def __init__(self):
        self.state = {
            'freethrowmade': 0,
            'twopointmade': 0,
            'threepointmade': 0,
            'rebound': 0,
            'assist': 0,
            'steal': 0,
            'turnover': 0,
            'points': 0
        }

    def update(self, type, value):
        self.state[type] += value

def process_file(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

        # TODO: process static game data
        game_id = data['id']
        home_team = data['home']['id']
        away_team = data['away']['id']

        team_stats = {}
        team_stats[home_team] = TeamStats()
        team_stats[away_team] = TeamStats()

        # process game events
        for period in data['periods']:
            period_label = period['type'] + "_" + str(period['number'])
            for event in period['events']:
                event_type = event['event_type']
                if event_type in interesting_events:
                    for stat in event['statistics']:
                        team_id = stat['team']['id']
                        event_type = stat['type']
                        process_statistic(team_id, stat, team_stats)

                        team_feature = {}
                        team_feature['period'] = period_label
                        team_feature['game_clock'] = clock_to_int(event['clock'])
                        team_feature['team_stats'] = team_stats[team_id].state
                        team_feature['team_id'] = team_id
                        team_feature['event_type'] = event_type
                        team_feature['game_id'] = game_id
                        team_feature['event_id'] = event['id']
                        save_event(team_feature)

        # # output final game model
        # for team in team_stats:
        #     print "team: %s , stats: %s" % (team, team_stats[team].state)
def save_event(feature):
    insert_cql = """
    INSERT INTO events(period, clock, team_id, event_type, game_id,
        event_id, ast, tov, stl, trb, three, ft, pt, two)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """
    session.execute(insert_cql, (
        feature['period'],
        feature['game_clock'],
        feature['team_id'],
        feature['event_type'],
        feature['game_id'],
        feature['event_id'],
        feature['team_stats']['assist'],
        feature['team_stats']['turnover'],
        feature['team_stats']['steal'],
        feature['team_stats']['rebound'],
        feature['team_stats']['threepointmade'],
        feature['team_stats']['freethrowmade'],
        feature['team_stats']['points'],
        feature['team_stats']['twopointmade'])
    )

def clock_to_int(clock):
    (m, s) = clock.split(':')
    return int(m) * 60 + int(s)

def process_statistic(team_id, stat, team_stats):
    stat_type = stat['type']
    if stat_type == 'freethrow':
        team_stats[team_id].update('freethrowmade', 1)
        team_stats[team_id].update('points', 1)
    elif stat_type == 'fieldgoal':
        if stat['points'] == 2:
            team_stats[team_id].update('twopointmade', 1)
            team_stats[team_id].update('points', 2)
        else:
            team_stats[team_id].update('threepointmade', 1)
            team_stats[team_id].update('points', 3)
    else:
        team_stats[team_id].update(stat_type, 1)

if __name__ == "__main__":
    args = sys.argv
    filename = str(args[1])
    process_file(filename)

