#!/usr/bin/env python
# Parses json formatted play-by-play Game data from SportRadar
#
# Usage: ./data_parser <play-by-play_json_file>

import sys
import json

class TeamStats(object):
    def __init__(self):
        self.state = {
            'freethrowmade': 0,
            'twopointmade': 0,
            'threepointmade': 0,
            'rebound': 0,
            'assist': 0,
            'steal': 0,
            'turnover': 0,
            'points': 0
        }

    def update(self, type, value):
        self.state[type] += value

interesting_events = ['freethrowmade', 'twopointmade', 'threepointmade',
                        'rebound', 'turnover']

abbrevs = {
        "Hawks":"ATL", 
        "Nets":"BKN",
        "Celtics":"BOS",
        "Hornets":"CHA",
        "Bobcats":"CHA",
        "Bulls":"CHI",
        "Cavaliers":"CLE", 
        "Mavericks":"DAL", 
        "Nuggets":"DEN", 
        "Pistons":"DET", 
        "Warriors":"GSW", 
        "Rockets":"HOU",
        "Pacers":"IND", 
        "Clippers":"LAC", 
        "Lakers":"LAL", 
        "Grizzlies":"MEM", 
        "Heat":"MIA",
        "Bucks":"MIL", 
        "Timberwolves":"MIN", 
        "Pelicans":"NOP",
        "Knicks":"NYK",
        "Thunder":"OKC", 
        "Magic":"ORL",
        "76ers":"PHI",
        "Suns":"PHX",
        "Trail Blazers":"POR", 
        "Kings":"SAC", 
        "Spurs":"SAS", 
        "Raptors":"TOR", 
        "Jazz":"UTA", 
        "Wizards":"WAS"
        }

def get_abbrev(team_name):
    return abbrevs[team_name]

def process_file(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

        # TODO: process static game data
        game_id = data['id']
        home_team = data['home']['id']
        away_team = data['away']['id']

        team_stats = {}
        team_stats[home_team] = TeamStats()
        team_stats[away_team] = TeamStats()

        # process game events
        for period in data['periods']:
            period_label = period['type'] + "_" + str(period['number'])
            for event in period['events']:
                event_type = event['event_type']
                if event_type in interesting_events:
                    for stat in event['statistics']:
                        team_id = stat['team']['id']
                        event_type = stat['type']
                        process_statistic(team_id, stat, team_stats)

                        team_feature = {}
                        team_feature['period'] = period_label
                        team_feature['game_clock'] = clock_to_int(event['clock'])
                        team_feature['team_stats'] = team_stats[team_id].state
                        team_feature['team_id'] = team_id
                        team_feature['event_type'] = event_type
                        team_feature['team_name'] = stat['team']['name']
                        team_feature['team_market'] = stat['team']['market']
                        team_feature['team_abbrev'] = get_abbrev(stat['team']['name'])
                        team_feature['game_id'] = game_id
                        team_feature['event_id'] = event['id']
                        team_feature['adv'] = 'home' if team_id == home_team else 'away'
                        team_feature['description'] = event['description']
                        print json.dumps(team_feature)

        # # output final game model
        # for team in team_stats:
        #     print "team: %s , stats: %s" % (team, team_stats[team].state)
def clock_to_int(clock):
    (m, s) = clock.split(':')
    return int(m) * 60 + int(s)

def process_statistic(team_id, stat, team_stats):
    stat_type = stat['type']
    if stat_type == 'freethrow':
        team_stats[team_id].update('freethrowmade', 1)
        team_stats[team_id].update('points', 1)
    elif stat_type == 'fieldgoal':
        if stat['points'] == 2:
            team_stats[team_id].update('twopointmade', 1)
            team_stats[team_id].update('points', 2)
        else:
            team_stats[team_id].update('threepointmade', 1)
            team_stats[team_id].update('points', 3)
    else:
        team_stats[team_id].update(stat_type, 1)

if __name__ == "__main__":
    args = sys.argv
    filename = str(args[1])
    process_file(filename)

