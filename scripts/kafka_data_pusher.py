#!/usr/bin/env python
# Pushes events from file passed in to Kafka cluster one line at a time
# Opted not to do bulk pushing because that isn't the nature of
# NBA play-by-play data
#
# Usage: ./kafka_data_pusher <kafka_cluster_host> <topic-name> <events_file>

import random
import sys
import logging
from kafka.client import KafkaClient
from kafka.producer import SimpleProducer
from time import sleep
import json

class Producer(object):

    def __init__(self, addr):
        self.client = KafkaClient(addr)
        self.producer = SimpleProducer(self.client)

    def produce_msgs(self, topic, delay, event_file):
        with open(event_file) as events:
            for line in events:
                self.producer.send_messages(topic, line.strip())
                print "Produced event... now sleeping for %s sec" % (delay)
                sleep(int(delay))

if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.WARNING
    )
    args = sys.argv
    ip_addr = str(args[1])
    topic = str(args[2])
    delay = str(args[3])
    event_file = str(args[4])
    prod = Producer(ip_addr)
    prod.produce_msgs(topic, delay, event_file)
