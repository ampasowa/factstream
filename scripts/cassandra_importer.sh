#!/bin/bash
# Parses a folder of NBA play-by-play data and outputs feature vectors
# into cassandra
#
# Usage: ./cassandra_importer <input_folder>

input_folder=$1

for file in "${input_folder}"/*
do
	python data_parser_cassandra.py "${file}"
done

