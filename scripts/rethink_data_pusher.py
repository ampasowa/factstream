#!/usr/bin/env python
# Pushes events from file passed in to Rethink DB one line at a time
#
# Usage: ./rethink_data_pusher <host> <db_name> <table_name> <delay> <events_file>

import random
import sys
import logging
import rethinkdb as r
from time import sleep
import json

class Producer(object):

    def __init__(self, hostname, db_name):
        self.conn = r.connect(host=hostname, db=db_name)

    def produce_msgs(self, table_name, delay, event_file):
        with open(event_file) as events:
            for line in events:
                r.table(table_name).insert(json.loads(line.strip())).run(self.conn)
                print "Produced event... now sleeping for %s sec" % (delay)
                sleep(int(delay))

if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.WARNING
    )
    args = sys.argv
    ip_addr = str(args[1])
    db_name = str(args[2])
    table_name = str(args[3])
    delay = str(args[4])
    event_file = str(args[5])
    prod = Producer(ip_addr, db_name)
    prod.produce_msgs(table_name, delay, event_file)

