#!/bin/bash
# Parses a folder of NBA play-by-play data and outputs feature vectors
# to a specified output folder
#
# Usage: ./parse_all_raw_data <input_folder> <output_folder>

input_folder=$1
output_folder=$2

for file in "${input_folder}"/*
do
	output_filename=$(basename "${file}" .json)
	#echo "processing ${file}"
	python game_summary_parser.py "${file}" > "${output_folder}/${output_filename}.txt"
done