#!/usr/bin/env python
# Parses json formatted schedule information from SportRadar
#
# Usage: ./game_id_extractor <schedule_json_file> > <game_ids.txt>

import sys
import json

def process_file(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

        # process game listings
        for game in data['games']:
            print game['id']

if __name__ == "__main__":
    args = sys.argv
    filename = str(args[1])
    process_file(filename)

