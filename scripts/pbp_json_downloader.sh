#!/bin/bash
# Takes in a list of game id's and downloads a list of all
# json formatted play-by-play data for each game using
# API key from SportRadar
#
# Usage: ./pbp_json_downloader <game_id_list_file> <API_KEY>

game_list=$1
api_key=$2
base_url="http://api.sportradar.us/nba-t3/games"

for game_id in $(cat $game_list);
do
	url="${base_url}/${game_id}/pbp.json?api_key=${api_key}"
	wget "$url" -O "../data/pbp/${game_id}.json"
	sleep 0.05
done
