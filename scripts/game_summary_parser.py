#!/usr/bin/env python
# Parses json formatted play-by-play Game data from SportRadar
#
# Usage: ./game_summary_parser <play-by-play_json_file>

import sys
import json

def process_file(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

        game_info = {}
        game_info['game_id'] = data['id']
        game_info['full_date'] = data['scheduled']
        game_info['date'] = data['scheduled'][:10]
        game_info['home_id'] = data['home']['id']
        game_info['home_name'] = data['home']['name']
        game_info['home_market'] = data['home']['market']
        game_info['home_points'] = data['home']['points']
        game_info['away_id'] = data['away']['id']
        game_info['away_name'] = data['away']['name']
        game_info['away_market'] = data['away']['market']
        game_info['away_points'] = data['away']['points']

        print json.dumps(game_info)

if __name__ == "__main__":
    args = sys.argv
    filename = str(args[1])
    process_file(filename)

