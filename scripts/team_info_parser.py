#!/usr/bin/env python
# Parses json formatted play-by-play Game data from SportRadar
#
# Usage: ./data_parser <play-by-play_json_file>

import sys
import json

def process_file(filename):
    with open(filename) as json_file:
        data = json.load(json_file)

        team_info = {}
        for conference in data['conferences']:
            conf_name = conference['alias']
            for division in conference['divisions']:
                div_name = division['name']
                for team in division['teams']:
                    team_info['id'] = team['id']
                    team_info['market'] = team['market']
                    team_info['name'] = team['name']
                    team_info['alias'] = team['alias']
                    print json.dumps(team_info)

if __name__ == "__main__":
    args = sys.argv
    filename = str(args[1])
    process_file(filename)

