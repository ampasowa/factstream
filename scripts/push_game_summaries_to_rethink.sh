#!/bin/bash
# Parses a folder of NBA play-by-play data and outputs feature vectors
# to a specified output folder
#
# Usage: ./parse_game_summaries_to_rethink <input_folder>

input_folder=$1

for file in "${input_folder}"/*
do
	output_filename=$(basename "${file}")
	echo "processing ${file}"
    python rethink_data_pusher.py "ec2-54-215-241-191.us-west-1.compute.amazonaws.com" "game_data" "game_info" "0" "${file}"
done
