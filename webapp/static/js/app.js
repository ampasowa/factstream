window.onload = function() {
    var data = {
        stats: {
            teams: [
            {
                url: '#',
                loc: 'Away',
                abbrev: 'AWAY',
                name: 'Team',
                pts: 0,
                ptsQtrs: [0, 0, 0, 0],
                fg: 0,
                tp: 0,
                thrp: 0,
                ft: 0,
                trb: 0,
                ast: 0,
                tov: 0,
                stl: 0
            }, {
                url: '#',
                loc: 'Home',
                abbrev: 'HOME',
                name: 'Team',
                pts: 0,
                ptsQtrs: [0, 0, 0, 0],
                fg: 0,
                tp: 0,
                thrp: 0,
                ft: 0,
                trb: 0,
                ast: 0,
                tov: 0,
                stl: 0
            }
            ]
        },
        playByPlay: [],
        factoid: [],
        qtr: ""
    };

    var viewModel = ko.mapping.fromJS(data);

    // Activates knockout.js
    ko.applyBindings(viewModel);
//{"adv": "home", "event_type": "rebound", "event_id": "c9cfc485-7e13-43d7-be07-ea8e653dcab1", "period": "quarter_1", "team_stats": {"rebound": 1, "assist": 0, "freethrowmade": 0, "threepointmade": 0, "points": 0, "twopointmade": 0, "steal": 0, "turnover": 0}, "team_id": "583ec928-fb46-11e1-82cb-f4ce4684ea4c", "team_market": "Detroit", "game_id": "fef7e2d8-30c9-4740-946c-0d7b0a9cd19e", "team_name": "Pistons", "game_clock": 699, "id": "93e16b5a-0524-40ee-b5f1-a2f7f8f51276"}

    var int_to_clock = function(clock){
        min = Math.floor(clock/60);
        sec = clock % 60;
        fsec = sec < 10 ? '0' + sec : sec;
        return min + ':' + fsec;
    } 

    var socket = io.connect('http://' + document.domain + ':' + location.port + '/stream');

    socket.on('away_event', function(msg) {
        viewModel.stats.teams()[0].loc(msg['team_market']);
        viewModel.stats.teams()[0].abbrev(msg['team_abbrev']);
        viewModel.stats.teams()[0].name(msg['team_name']);
        viewModel.stats.teams()[0].pts(msg['team_stats']['points']);
        viewModel.stats.teams()[0].fg(parseInt(msg['team_stats']['twopointmade']) + parseInt(msg['team_stats']['threepointmade']));
        viewModel.stats.teams()[0].tp(msg['team_stats']['twopointmade']);
        viewModel.stats.teams()[0].thrp(msg['team_stats']['threepointmade']);
        viewModel.stats.teams()[0].ft(msg['team_stats']['freethrowmade']);
        viewModel.stats.teams()[0].trb(msg['team_stats']['rebound']);
        viewModel.stats.teams()[0].ast(msg['team_stats']['assist']);
        viewModel.stats.teams()[0].tov(msg['team_stats']['turnover']);
        viewModel.stats.teams()[0].stl(msg['team_stats']['steal']);
        viewModel.playByPlay.unshift(int_to_clock(msg['game_clock']) + " - " + msg['team_abbrev'] + ' - ' + msg['description'])
        period = msg['period']
        if (period == 'quarter_1') {
            viewModel.qtr("-- 1st quarter");
        } else if (period == 'quarter_2') {
            viewModel.qtr("-- 2nd quarter");
        } else if (period == 'quarter_3') {
            viewModel.qtr("-- 3rd quarter");
        } else if (period == 'quarter_4') {
            viewModel.qtr("-- 4th quarter");
        }
    });
    socket.on('home_event', function(msg) {
        viewModel.stats.teams()[1].loc(msg['team_market']);
        viewModel.stats.teams()[1].abbrev(msg['team_abbrev']);
        viewModel.stats.teams()[1].name(msg['team_name']);
        viewModel.stats.teams()[1].pts(msg['team_stats']['points']);
        viewModel.stats.teams()[1].fg(parseInt(msg['team_stats']['twopointmade']) + parseInt(msg['team_stats']['threepointmade']));
        viewModel.stats.teams()[1].tp(msg['team_stats']['twopointmade']);
        viewModel.stats.teams()[1].thrp(msg['team_stats']['threepointmade']);
        viewModel.stats.teams()[1].ft(msg['team_stats']['freethrowmade']);
        viewModel.stats.teams()[1].trb(msg['team_stats']['rebound']);
        viewModel.stats.teams()[1].ast(msg['team_stats']['assist']);
        viewModel.stats.teams()[1].tov(msg['team_stats']['turnover']);
        viewModel.stats.teams()[1].stl(msg['team_stats']['steal']);
        viewModel.playByPlay.unshift(int_to_clock(msg['game_clock']) + " - " + msg['team_abbrev'] + ' - ' + msg['description'])
        period = msg['period']
        if (period == 'quarter_1') {
            viewModel.qtr("-- 1st quarter");
        } else if (period == 'quarter_2') {
            viewModel.qtr("-- 2nd quarter");
        } else if (period == 'quarter_3') {
            viewModel.qtr("-- 3rd quarter");
        } else if (period == 'quarter_4') {
            viewModel.qtr("-- 4th quarter");
        }
    });
    socket.on('factoid', function(msg) {
       viewModel.factoid.unshift(msg);
    });
}

