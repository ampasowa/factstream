#!/usr/bin/env python
import threading
from threading import Thread
from flask import jsonify
from flask import render_template
from flask import Flask, redirect
from flask.ext.socketio import SocketIO, emit
from time import sleep
import json
import rethinkdb as r

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
conn = r.connect(host='54.215.241.191', port=28015, db='game_data')
conn2 = r.connect(host='54.215.241.191', port=28015, db='game_data')
thread = Thread() # background thread for receiving raw events
threadFact = Thread() #background thread for receiving potentially interesting events
fact_dict = dict()

def processed_events_listener():
    print("inside processed_event_listener")
    feed = r.table('processed_events').changes().run(conn)
    for change in feed:
        event = change['new_val']
        #print json.dumps(event)
        if event['adv'] == 'home':
            #print "emitting home event"
            socketio.emit('home_event', event, namespace='/stream')
        elif event['adv'] == 'away':
            #print "emitting away event"
            socketio.emit('away_event', event, namespace='/stream')
        else:
            print("Badly formatted event. No 'adv' field")

def factoids_events_listener():
    print("inside factoids_events_listener")
    feed = r.table('events').changes().run(conn2)
    for change in feed:
        event = change['new_val']
        print json.dumps(event)
        if event['curr_game_id'] not in fact_dict:
            fact_dict[event['curr_game_id']] = set()

        factoid = getFactoid(event)
        if factoid is not None:
            print "emitting factoid"
            socketio.emit('factoid', factoid, namespace='/stream')

def getFactoid(event):
    game_id = event['curr_game_id']
    curr_pt = int(event['curr_pt'])
    curr_ast = int(event['curr_ast'])
    curr_trb = int(event['curr_trb'])
    hist_pt = int(event['hist_pt'])
    hist_ast = int(event['hist_ast'])
    hist_trb = int(event['hist_trb'])
    period = event['curr_period']
    curr_clock = int(event['curr_clock'])
    team = get_team(event['hist_team_id'])
    game = get_game(event['hist_game_id'])
    final_score = get_final_score(game)

    if period == 'quarter_1':
        if curr_ast >= 10 and curr_pt >= 35:
            fact = "Last team with at least 10 assists and 35 points in the 1st quarter was the %s on %s. Final score was %s" % (team, game['date'], final_score)
            data_tuple = (10, 35)
            if data_tuple not in fact_dict[game_id]:
                fact_dict[game_id].add(data_tuple)
                return fact

    return None

def get_game(game_id):
    cursor = r.table('game_info').filter({'game_id': game_id}).run(conn2)
    return cursor.next()

def get_team(team_id):
    return r.table('team_info').get(team_id).run(conn2)['name']

def get_final_score(game):
    return "%s %s - %s %s" % (abbrev(game['away_id']), game['away_points'], abbrev(game['home_id']), game['home_points'])
    
def abbrev(team_id):
    return r.table('team_info').get(team_id).run(conn2)['alias']

@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html")

@app.route('/live_game')
def live_game():
    return render_template("live_game.html")

@app.route("/slides")
def slides():
    return render_template("slides.html")

@app.route("/robots.txt")
def robots():
    return render_template("robots.txt")

@app.route('/_start')
def start_pushing():
    return redirect("http://ec2-54-183-107-47.us-west-1.compute.amazonaws.com:5000/_start", code=302)

@app.route('/_stop')
def stop_pushing():
    return redirect("http://ec2-54-183-107-47.us-west-1.compute.amazonaws.com:5000/_stop", code=302)

@socketio.on('connect', namespace='/stream')
def test_connect():
    global thread
    global threadFact
    print('Client connected')

    if not thread.isAlive():
        print "Starting all-events background thread"
        thread = threading.Thread(target=processed_events_listener)
        thread.daemon = True
        thread.start()

    if not threadFact.isAlive():
        print "Starting factoids background thread"
        threadFact = threading.Thread(target=factoids_events_listener)
        threadFact.daemon = True
        threadFact.start()

@socketio.on('disconnect', namespace='/stream')
def test_disconnect():
    print('Client disconnected')

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=80)

