#FactStream
A Realtime NBA Trivia Generator

### About
FactStream is a Data Engineering project for the [Insight Data Engineering](http://www.insightdataengineering.com) fellowship program 2015c session.

### Introduction
FactStream analyzes a realtime stream of live NBA play-by-play data and attempts to surface interesting factoids about the events in the game, and to put these facts in a historical context.

### Website
The website is currently hosted at http://www.project-factstream.com

### Pipeline
The pipeline loosely follows the principles of the Lambda Architecture. 

- The ingestion layer accepts a stream of play-by-play data from external sources through Kafka.
- From Kafka, these events are relayed to both the batch layer (HDFS) and into the streaming layer (Storm)
- The batch layer performs a simple parsing operation of the play-by-play data and computes aggregates, which are stored as timestamped feature vectors in Cassandra for fast serving
- The streaming layer takes in each real-time event and computes the similarity between that event and all historical events around the same timestamp in the game, using Cosine similarity as a distance function
- If the similarity is greater than some threshold, then the event is potentially interesting and is pushed further down the pipeline to RethinkDB
- At the serving layer, RethinkDB pushes these potentially interesting events to the users and allows further filtering through an expressive SQL-like language, ReQL
- These results are seved to the user via a Flask web application

