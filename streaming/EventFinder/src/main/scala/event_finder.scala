import kafka.serializer.StringDecoder

import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql._
import com.datastax.spark.connector._

object GameDataStreaming {

  def main(args: Array[String]) {

    val brokers = "localhost:9092"
    val topics = "my-topic-2"
    val topicsSet = topics.split(",").toSet

    // Create context with 2 second batch interval
    val sparkConf = new SparkConf().setAppName("game_data").set("spark.cassandra.connection.host", "127.0.0.1")
    val sc = new SparkContext(sparkConf)
    val ssc = new StreamingContext(sc, Seconds(10))

    // Create direct kafka stream with brokers and topics
    val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers)
    val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicsSet)

    // Load processed event data from cassandra
    val event_data = sc.cassandraTable("game_data", "events").cache()

    // Get the lines and show results
    val filtered_messages = messages.map { line =>
      val sqlContext = SQLContextSingleton.getInstance(sc)
      import sqlContext.implicits._
      println("Processing New RDD")
        val event = sc.parallelize(List(line._2))
        val new_event = sqlContext.jsonRDD(event).toDF
        val event_row = new_event.first()
        val clock_str = event_row.getString(0)
        val clock = clock_to_int(clock_str)
        val period_val = event_row.getString(2)
        new_event.show

        val filtered_events = event_data.where("period = ?", period_val)
                                        .where("clock >= ?", (clock - 60))
                                        .where("clock <= ?", (clock + 60))

        val joined_events = filtered_events.cartesian(new_event.rdd)
        val similarity = joined_events.map(row => {
          val old_row = row._1
          val new_row = row._2
          val old_arr = Array(old_row.getLong("ast"), old_row.getLong("tov"),
            old_row.getLong("stl"), old_row.getLong("trb"),
            old_row.getLong("three"), old_row.getLong("ft"),
            old_row.getLong("pt"), old_row.getLong("two"))
          val new_arr = new_row.getStruct(4).toSeq.toArray.map(_.asInstanceOf[Long])

          CosineSimilarity.cosineSimilarity(old_arr, new_arr)
        })
      println("Done processing RDD")
    }
    // Start the computation
    ssc.start()
    ssc.awaitTermination()
  }

  def clock_to_int(clock: String): Int = {
    val arr = clock.split(":")
    arr(0).toInt * 60 + arr(1).toInt
  }
}

/** Lazily instantiated singleton instance of SQLContext */
object SQLContextSingleton {

  @transient  private var instance: SQLContext = _

  def getInstance(sparkContext: SparkContext): SQLContext = {
    if (instance == null) {
      instance = new SQLContext(sparkContext)
    }
    instance
  }
}
