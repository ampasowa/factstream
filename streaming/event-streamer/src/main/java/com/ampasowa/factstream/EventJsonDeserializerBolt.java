package com.ampasowa.factstream;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

public class EventJsonDeserializerBolt extends BaseRichBolt {

	private static final long serialVersionUID = 5867138982354070231L;

	OutputCollector _collector;

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void execute(Tuple tuple) {
		JsonObject event = Json.parse(tuple.getString(0)).asObject();
		JsonObject teamStats = event.get("team_stats").asObject();
		_collector.emit(tuple,
				new Values(event.get("period").asString(),
						event.get("game_clock").asInt(),
						event.get("team_id").asString(),
						event.get("event_type").asString(),
						event.get("game_id").asString(),
						event.get("event_id").asString(),
						teamStats.get("assist").asInt(),
						teamStats.get("freethrowmade").asInt(),
						teamStats.get("points").asInt(),
						teamStats.get("steal").asInt(),
						teamStats.get("threepointmade").asInt(),
						teamStats.get("turnover").asInt(),
						teamStats.get("rebound").asInt(),
						teamStats.get("twopointmade").asInt()
						));
		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("period", "clock", "team_id",
				"event_type", "game_id", "event_id", "ast", "ft", "pt",
				"stl", "three", "tov", "trb", "two"));
	}
}