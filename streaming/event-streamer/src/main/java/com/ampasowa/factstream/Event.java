package com.ampasowa.factstream;

import backtype.storm.tuple.Tuple;

import com.datastax.driver.core.Row;

/**
 * Representation of event model
 *
 * Database Schema:
 * period | clock | team_id | event_type | game_id | event_id |
 * ast | ft | pt | stl | three | tov | trb | two
 *
 * @author nii
 *
 */
public class Event {
	private final String period;
	private final int clock;
	private final String teamId;
	private final String eventType;
	private final String gameId;
	private final String eventId;
	private final int points;
	private final int rebounds;
	private final int assists;

	public Event(Row dbRow) {
		this(dbRow.getString("period"),
				dbRow.getInt("clock"),
				dbRow.getString("team_id"),
				dbRow.getString("event_type"),
				dbRow.getString("game_id"),
				dbRow.getString("event_id"),
				dbRow.getInt("pt"),
				dbRow.getInt("trb"),
				dbRow.getInt("ast"));
	}

	public Event(Tuple tuple) {
		this(tuple.getStringByField("period"),
				tuple.getIntegerByField("clock").intValue(),
				tuple.getStringByField("team_id"),
				tuple.getStringByField("event_type"),
				tuple.getStringByField("game_id"),
				tuple.getStringByField("event_id"),
				tuple.getIntegerByField("pt").intValue(),
				tuple.getIntegerByField("trb").intValue(),
				tuple.getIntegerByField("ast").intValue());
	}

	public Event(String period, int clock, String teamId, String eventType,
			String gameId, String eventId, int points, int rebounds, int assists) {
		this.period = period;
		this.clock = clock;
		this.teamId = teamId;
		this.eventType = eventType;
		this.gameId = gameId;
		this.eventId = eventId;
		this.points = points;
		this.rebounds = rebounds;
		this.assists = assists;
	}

	public int[] getFeatureVector() {
		return new int[]{points, rebounds, assists};
	}

	public String getPeriod() {
		return period;
	}

	public int getClock() {
		return clock;
	}

	public String getTeamId() {
		return teamId;
	}

	public String getEventType() {
		return eventType;
	}

	public String getGameId() {
		return gameId;
	}

	public String getEventId() {
		return eventId;
	}

	public int getPoints() {
		return points;
	}

	public int getRebounds() {
		return rebounds;
	}

	public int getAssists() {
		return assists;
	}

}
