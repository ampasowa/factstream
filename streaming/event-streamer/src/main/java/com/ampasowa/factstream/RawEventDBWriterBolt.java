package com.ampasowa.factstream;

import java.util.Map;

import backtype.storm.task.ShellBolt;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;

public class RawEventDBWriterBolt extends ShellBolt implements IRichBolt {

	private static final long serialVersionUID = 3543815744296607404L;

	public RawEventDBWriterBolt() {
		super("python", "raw_event_db_writer.py");
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		return null;
	}
}
