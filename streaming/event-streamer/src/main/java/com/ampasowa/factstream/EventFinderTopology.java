package com.ampasowa.factstream;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.StringScheme;
import storm.kafka.ZkHosts;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;

public class EventFinderTopology {
	private final BrokerHosts brokerHosts;
	public static final Logger LOG = LoggerFactory.getLogger(EventFinderTopology.class);

	public EventFinderTopology(String kafkaZookeeper) {
		brokerHosts = new ZkHosts(kafkaZookeeper);
	}

	public StormTopology buildTopology() {
		// Setup Kafka input bolt
		SpoutConfig kafkaConfig = new SpoutConfig(brokerHosts, "events-in-5", "/kafka-storm", "storm");
		kafkaConfig.scheme = new SchemeAsMultiScheme(new StringScheme());

		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("events-spout", new KafkaSpout(kafkaConfig), 1);
		builder.setBolt("raw-event-db-bolt", new RawEventDBWriterBolt(), 1).shuffleGrouping("events-spout");
		builder.setBolt("json-deserializer-bolt", new EventJsonDeserializerBolt(), 1).shuffleGrouping("events-spout");
		builder.setBolt("query-bolt-1", new RangeQueryBolt(-30, 30, 0.999), 1).shuffleGrouping("json-deserializer-bolt");
		builder.setBolt("query-bolt-2", new RangeQueryBolt(0, 30, 0.999), 1).shuffleGrouping("json-deserializer-bolt");
		builder.setBolt("db-writer-bolt-1", new EventDBWriterBolt(), 1).shuffleGrouping("query-bolt-1");
		builder.setBolt("db-writer-bolt-2", new EventDBWriterBolt(), 1).shuffleGrouping("query-bolt-2");

		return builder.createTopology();
	}

	public static void main(String[] args) throws Exception {
		String kafkaZk = args[0];
		EventFinderTopology eventFinderToplogy = new EventFinderTopology(kafkaZk);
		Config config = new Config();
		config.setDebug(false);
		StormTopology stormTopology = eventFinderToplogy.buildTopology();

		if (args != null && args.length > 1) {
			String name = args[1];
			String nimbusHost = args[2];
			config.setNumWorkers(3);
			config.setMaxTaskParallelism(5);
			config.put(Config.NIMBUS_HOST, nimbusHost);
			config.put(Config.NIMBUS_THRIFT_PORT, 6627);
			config.put(Config.STORM_ZOOKEEPER_PORT, 2181);
			config.put(Config.STORM_ZOOKEEPER_SERVERS, Arrays.asList(kafkaZk));
			StormSubmitter.submitTopology(name, config, stormTopology);
		} else {
			config.setMaxTaskParallelism(2);
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("test", config, stormTopology);
		}
	}
}
