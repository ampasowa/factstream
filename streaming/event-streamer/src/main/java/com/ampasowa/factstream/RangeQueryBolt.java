package com.ampasowa.factstream;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class RangeQueryBolt extends BaseRichBolt {

	private static final long serialVersionUID = -7005346007522745978L;
	public static final Logger LOG = LoggerFactory.getLogger(RangeQueryBolt.class);
	private Cluster cluster;
	private Session session;
	private final int clockLowerDelta;
	private final int clockRange;
	private final double filterThreshold;
	OutputCollector _collector;

	public RangeQueryBolt(int clockLowerDelta, int clockRange, double filterThreshold) {
		this.clockLowerDelta = clockLowerDelta;
		this.clockRange = clockRange;
		this.filterThreshold = filterThreshold;
	}

	@Override
	public void prepare(@SuppressWarnings("rawtypes") Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
		cluster = Cluster.builder().addContactPoint("ip-172-31-14-183").build();
		session = cluster.connect("game_data");
	}

	@Override
	public void execute(Tuple tuple) {
		Event curr = new Event(tuple);
		String period = curr.getPeriod();
		int clock = curr.getClock();
		int[] currFeat = curr.getFeatureVector();

		int clockLower = clock + clockLowerDelta;
		int clockUpper = clockLower + clockRange;

		// TODO: Change to use prepared statement
		String cqlStatement = "SELECT * FROM events WHERE period = ? AND clock >= ? AND clock < ? limit 200;";
		ResultSet results = session.execute(cqlStatement, period, clockLower, clockUpper);
		for (Row row : results) {
			Event hist = new Event(row);
			int[] histFeat = hist.getFeatureVector();
			double sim = Similarity.cosine(currFeat, histFeat);
			if (sim > filterThreshold && !curr.getGameId().equals(hist.getGameId())) {
				_collector.emit(createValues(sim, hist, curr));
			}
		}

		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("similarity", "hist_period", "hist_game_id",
				"hist_team_id", "hist_clock", "hist_pt", "hist_trb", "hist_ast",
				"curr_period", "curr_game_id", "curr_team_id", "curr_clock",
				"curr_pt", "curr_trb", "curr_ast"));
	}

	@Override
	public void cleanup() {
		cluster.close();
	}

	private Values createValues(double sim, Event hist, Event curr) {
		return new Values(sim, hist.getPeriod(), hist.getGameId(), hist.getTeamId(),
				hist.getClock(), hist.getPoints(), hist.getRebounds(), hist.getAssists(),
				curr.getPeriod(), curr.getGameId(), curr.getTeamId(), curr.getClock(),
				curr.getPoints(), curr.getRebounds(), curr.getAssists());
	}

}
