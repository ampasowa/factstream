# Takes in a string tuple and deserializes it into its constituent parts
#
# DB Event Schema
# period | clock | team_id | event_type | game_id | event_id |
# ast | ft | pt | stl | three | tov | trb | two
#
# JSON sample
# {"event_type": "rebound", "event_id": "55b54f32-0bd8-46b4-bce8-5833189973d1",
# "period": "quarter_1", "team_stats": {"assist": 0, "turnover": 0, "steal": 0,
# "rebound": 1, "threepointmade": 0, "freethrowmade": 0, "points": 0, "twopointmade": 0},
# "team_id": "583ecdfb-fb46-11e1-82cb-f4ce4684ea4c",
# "game_id": "6b82bbb2-44c9-4310-9849-2e97ebc9e972", "game_clock": 701}

import storm
import json

def is_valid_json(myjson):
    try:
        json_object = json.loads(myjson)
    except ValueError, e:
        return False
    return True

class EventDeserializerBolt(storm.BasicBolt):
    def process(self, tup):
        message = tup.values[0]
        if is_valid_json(message):
            event = json.loads(message)
            period = event['period']
            clock = event['game_clock']
            team_id = event['team_id']
            event_type = event['event_type']
            game_id = event['game_id']
            event_id = event['event_id']
            ast = event['team_stats']['assist']
            ft = event['team_stats']['freethrowmade']
            pt = event['team_stats']['points']
            stl = event['team_stats']['steal']
            three = event['team_stats']['threepointmade']
            tov = event['team_stats']['turnover']
            trb = event['team_stats']['rebound']
            two = event['team_stats']['twopointmade']

            storm.emit([period, clock, team_id, event_type, game_id,
                        event_id, ast, ft, pt, stl, three, tov, trb, two])
            storm.ack(tup)

EventDeserializerBolt().run()

