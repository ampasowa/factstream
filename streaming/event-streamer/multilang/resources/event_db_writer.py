# Writes output of data computed to be interesting to rethinkDB

import storm
import rethinkdb as r
import json

class EventDbWriter(storm.BasicBolt):

    def initialize(self, conf, context):
        self.conn = r.connect(host='ip-172-31-11-135', db='game_data')

    def process(self, tup):
        event = {}
        event['similarity'] = tup.values[0]
        event['hist_period'] = tup.values[1]
        event['hist_game_id'] = tup.values[2]
        event['hist_team_id'] = tup.values[3]
        event['hist_clock'] = tup.values[4]
        event['hist_pt'] = tup.values[5]
        event['hist_trb'] = tup.values[6]
        event['hist_ast'] = tup.values[7]
        event['curr_period'] = tup.values[8]
        event['curr_game_id'] = tup.values[9]
        event['curr_team_id'] = tup.values[10]
        event['curr_clock'] = tup.values[11]
        event['curr_pt'] = tup.values[12]
        event['curr_trb'] = tup.values[13]
        event['curr_ast'] = tup.values[14]

        r.table('events').insert(event).run(self.conn)

EventDbWriter().run()

