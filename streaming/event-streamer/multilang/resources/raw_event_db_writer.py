# Writes output of data computed to be interesting to rethinkDB

import storm
import rethinkdb as r
import json

class RawEventDbWriter(storm.BasicBolt):

    def initialize(self, conf, context):
        self.conn = r.connect(host='ip-172-31-11-135', db='game_data')

    def process(self, tup):
        event = tup.values[0]
        r.table('processed_events').insert(json.loads(event.strip())).run(self.conn)

RawEventDbWriter().run()
